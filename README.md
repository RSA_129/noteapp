# Simple Note App


## Description
Simple note app that allows to register, CRUD and search by title notes.

## Installation
- Create and edit file .env:
  - establish connection to db:
    - DB_NAME - db name 
    - DB_USERNAME - db user
    - DB_PASSWORD - db password
  - DEBUG_SQL - make true to see sql queries in output
- run maven build and execution
