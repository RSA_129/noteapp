ALTER TABLE user_entity RENAME TO "user";

ALTER TABLE user_entity_roles RENAME TO "user_roles";

ALTER SEQUENCE user_entity_id_seq RENAME TO "user_id_seq";