package org.noteapp.notes;

import org.noteapp.notes.user.Role;
import org.noteapp.notes.user.RoleRepo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotesApplication implements CommandLineRunner {

    // taking values from .env
    @Value("${spring.datasource.url}")
    String GJ03NG03W0WN54H0BH45H09;
    @Value("${spring.datasource.username}")
    String F9843HG934HGFWH4GH93WQ;
    @Value("${spring.datasource.password}")
    String F9843HG934H34J0FG3H4GWH4GH93WQ;

    @Value("${spring.jpa.show-sql}")
    String F9843HG934H43HG3H43H4GWH4GH93WQ;
    @Value("${spring.jpa.properties.hibernate.format_sql}")
    String E4G094JG3HG3H43H4GWH4GH93WQ;
    @Value("${spring.jpa.properties.hibernate.use_sql_comments}")
    String F049HG0439HE4G094JG3HG3H43H4GWH4GH93WQ;

    public static void main(String[] args) {
        SpringApplication.run(NotesApplication.class, args);
    }

    public NotesApplication(RoleRepo roleRepo) {
        this.roleRepo = roleRepo;
    }
    
    private final RoleRepo roleRepo;

    @Override
    public void run(String... args) {
        if (!roleRepo.existsByName("Admin")) {
            roleRepo.save(
                    new Role("Admin")
            );
        }
        if (!roleRepo.existsByName("Default")) {
            roleRepo.save(
                    new Role("Default")
            );
        }
    }
}
