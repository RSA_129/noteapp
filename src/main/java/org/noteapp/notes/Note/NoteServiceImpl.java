package org.noteapp.notes.Note;

import org.noteapp.notes.security.SecuritySession;
import org.noteapp.notes.user.UserDto;
import org.noteapp.notes.user.UserRepo;
import org.noteapp.notes.user.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class NoteServiceImpl implements NoteService {
    private final NoteRepo noteRepo;
    private final UserRepo userRepo;

    @Autowired
    public NoteServiceImpl(NoteRepo noteRepo, UserRepo userRepo) {
        this.noteRepo = noteRepo;
        this.userRepo = userRepo;
    }

    @Override
    public List<NoteDto> findAllNotes() {
        String username = SecuritySession.getUsername();
        if (username != null){
            List<Note> notes = noteRepo.findByOwner_UsernameIgnoreCase(username);
            return notes.stream().map(this::mapToNoteDto).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    @Override
    public void saveNote(NoteDto noteDto) {
        String username = SecuritySession.getUsername();
        UserDto user = UserServiceImpl.mapToUserDto(userRepo.findFirstByUsernameIgnoreCase(username));
        noteDto.setOwner(user);
        noteRepo.save(mapToNote(noteDto));
    }

    @Override
    public NoteDto findNoteById(long note_id) {
        Note note = noteRepo.findById(note_id).get();
        return mapToNoteDto(note);
    }

    @Override
    public void updateNote(NoteDto noteDto) {
        String username = SecuritySession.getUsername();
        UserDto user = UserServiceImpl.mapToUserDto(userRepo.findFirstByUsernameIgnoreCase(username));
        noteDto.setOwner(user);
        noteRepo.save(mapToNote(noteDto));
    }

    @Override
    public void delete(long noteId) {
        noteRepo.deleteById(noteId);
    }

    @Override
    public List<NoteDto> searchNotes(String query) {
        if (query.isBlank()) return findAllNotes();
        String username = SecuritySession.getUsername();
        if (username != null) {
            List<Note> notes = noteRepo.findByTitleContainsIgnoreCaseAndOwner_UsernameIgnoreCase(query, username);
            return notes.stream().map(this::mapToNoteDto).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    private Note mapToNote(NoteDto noteDto) {
        return Note.builder()
                .id(noteDto.getId())
                .title(noteDto.getTitle())
                .content(noteDto.getContent())
                .owner(UserServiceImpl.mapToUser(noteDto.getOwner()))
                .createdAt(noteDto.getCreatedAt())
                .updatedAt(noteDto.getUpdatedAt())
                .build();
    }

    private NoteDto mapToNoteDto(Note note) {
        return NoteDto.builder()
                .id(note.getId())
                .title(note.getTitle())
                .content(note.getContent())
                .owner(UserServiceImpl.mapToUserDto(note.getOwner()))
                .createdAt(note.getCreatedAt())
                .updatedAt(note.getUpdatedAt())
                .build();
    }


}
