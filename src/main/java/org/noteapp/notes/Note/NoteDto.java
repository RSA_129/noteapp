package org.noteapp.notes.Note;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Data;
import org.noteapp.notes.user.UserDto;

import java.time.LocalDateTime;

@Data
@Builder
public class NoteDto {
    private Long id;

    @Size(max = 20, message = "Title is too long")
    @NotEmpty(message = "Note title should not be empty")
    private String title;

    @Size(max = 2047, message = "Note content is too long")
    private String content;

    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    private UserDto owner;
}
