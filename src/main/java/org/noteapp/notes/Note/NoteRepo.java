package org.noteapp.notes.Note;

import org.noteapp.notes.Note.Note;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NoteRepo extends JpaRepository<Note, Long> {

    List<Note> findByOwner_UsernameIgnoreCase(String username);

    List<Note> findByTitleContainsIgnoreCaseAndOwner_UsernameIgnoreCase(String title, String username);
}
