package org.noteapp.notes.Note;

import jakarta.validation.Valid;
import org.noteapp.notes.security.SecuritySession;
import org.noteapp.notes.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@Controller
public class NoteController {
    private final NoteService noteService;
    private final UserService userService;

    @Autowired
    public NoteController(NoteService noteService, UserService userService) {
        this.noteService = noteService;
        this.userService = userService;
    }

    @GetMapping("/")
    public String index(Model model) {
        long amount_of_users = userService.count_users();
        model.addAttribute("amount_of_users", amount_of_users);
        return "index";
    }

    @GetMapping("/notes")
    public String notesList(Model model) {
        List<NoteDto> notesDto = noteService.findAllNotes();
        model.addAttribute("notes", notesDto);
        return "notes-list";
    }

    @GetMapping("/notes/search")
    public String searchNote(@RequestParam(value = "q") String query, Model model) {
        List<NoteDto> notesDto = noteService.searchNotes(query);
        model.addAttribute("notes", notesDto);
        return "notes-list";
    }

    @GetMapping("/notes/add")
    public String getNoteForm (Model model) {
        Note note = new Note();
        model.addAttribute("note", note);
        return "note-add";
    }

    @PostMapping("/notes/add")
    public String noteSave(@Valid @ModelAttribute("note") NoteDto noteDto, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("note", noteDto);
            return "note-add";
        }
        noteService.saveNote(noteDto);
        return "redirect:/notes";
    }

    @GetMapping("/notes/{id}")
    public String noteDetailPage (@PathVariable("id") long note_id, Model model) {
        NoteDto noteDto = noteService.findNoteById(note_id);
        if (noteDto != null && Objects.equals(noteDto.getOwner().getUsername(), SecuritySession.getUsername())) {
            model.addAttribute("note", noteDto);
            return "note-detail-page";
        }
        return "redirect:/notes";
    }

    @GetMapping("/notes/{id}/delete")
    public String noteDelete(@PathVariable("id") long note_id) {
        NoteDto noteDto = noteService.findNoteById(note_id);
        if (noteDto != null && Objects.equals(noteDto.getOwner().getUsername(), SecuritySession.getUsername())) {
            noteService.delete(note_id);
            return "redirect:/notes";
        }
        return "redirect:/notes";
    }

    @GetMapping("/notes/{id}/edit")
    public String editNoteForm (@PathVariable("id") long note_id, Model model) {
        NoteDto noteDto = noteService.findNoteById(note_id);
        if (noteDto != null && Objects.equals(noteDto.getOwner().getUsername(), SecuritySession.getUsername())) {
            model.addAttribute("note", noteDto);
            return "note-edit";
        }
        return "redirect:/notes";
    }

    @PostMapping("/notes/{id}/edit")
    public String updateNote(@PathVariable("id") long note_id,
                             @Valid @ModelAttribute("note") NoteDto note,
                             BindingResult result
    ) {
        if (result.hasErrors()) {
            return "note-edit";
        }
        NoteDto noteDto = noteService.findNoteById(note_id);
        if (noteDto != null && Objects.equals(noteDto.getOwner().getUsername(), SecuritySession.getUsername())) {
            note.setId(note_id);
            noteService.updateNote(note);
        }
        return "redirect:/notes";
    }

}
