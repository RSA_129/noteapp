package org.noteapp.notes.Note;

import java.util.List;

public interface NoteService {
    List<NoteDto> findAllNotes();
    void saveNote(NoteDto noteDto);

    NoteDto findNoteById(long note_id);

    void updateNote(NoteDto note);

    void delete(long noteId);

    List<NoteDto> searchNotes(String query);
}
