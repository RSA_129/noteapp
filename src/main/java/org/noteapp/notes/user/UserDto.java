package org.noteapp.notes.user;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.LinkedHashSet;
import java.util.Set;


@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class UserDto {
    private Long id;

    @Size(message = "Username is too long", max = 20)
    @NotEmpty(message = "Username should not be empty")
    private String username;

    @Email(message = "It should be email")
    @Size(message = "Email is too long", max = 63)
    @NotEmpty(message = "Email should not be empty")
    private String email;

    @NotEmpty(message = "Password should not be empty")
    private String password;

    private Set<Role> roles = new LinkedHashSet<>();

}