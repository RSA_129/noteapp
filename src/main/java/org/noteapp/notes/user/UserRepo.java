package org.noteapp.notes.user;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, Long> {
    User findFirstByEmailIgnoreCase(String email);

    User findFirstByUsernameIgnoreCase(String username);

}