package org.noteapp.notes.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class UserServiceImpl implements UserService {


    @Autowired
    public UserServiceImpl(UserRepo userRepo, PasswordEncoder passwordEncoder,
                           RoleRepo roleRepo) {
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
        this.roleRepo = roleRepo;
    }
    private final UserRepo userRepo;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepo roleRepo;

    @Override
    public void saveUser(UserDto userDto) {
        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setEmail(userDto.getEmail());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        Role role = roleRepo.findFirstByName("Default");
        user.setRoles(Set.of(role));
        userRepo.save(user);
    }

    @Override
    public UserDto findByEmail(String email) {
        User user = userRepo.findFirstByEmailIgnoreCase(email.strip());
        if (user != null) {
            return mapToUserDto(user);
        }
        return null;
    }

    @Override
    public UserDto findByUsername(String username) {
        User user = userRepo.findFirstByUsernameIgnoreCase(username.strip());
        if (user != null) {
            return mapToUserDto(user);
        }
        return null;
    }

    @Override
    public long count_users() {
        return userRepo.count();
    }

    public static User mapToUser(UserDto userDto) {
        return User.builder()
                .id(userDto.getId())
                .username(userDto.getUsername())
                .email(userDto.getEmail())
                .password(userDto.getPassword())
                .build();
    }
    public static UserDto mapToUserDto(User user) {
        return UserDto.builder()
                .id(user.getId())
                .username(user.getUsername())
                .email(user.getEmail())
                .password(user.getPassword())
                .build();
    }
}
