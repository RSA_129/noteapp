package org.noteapp.notes.user;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Role, Long> {

    boolean existsByName(String name);

    Role findFirstByName(String name);
}