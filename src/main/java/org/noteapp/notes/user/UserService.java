package org.noteapp.notes.user;

public interface UserService {
    void saveUser(UserDto userDto);

    UserDto findByEmail(String email);

    UserDto findByUsername(String username);

    long count_users();
}
